### Homework based on Well-Typed Haskell Course

TODO

- [ ] Do assignment A.hs
- [x] Do assignment B.hs [B.hs](./B.hs)
- [ ] Do assignment C.hs

#### Part 1 - Introduction
- [Link to part 1: Introduction](https://teaching.well-typed.com/intro/introduction.html)

#### Part 2 - Data types and Functions
- [Link to part 2: Data types and Functions](https://teaching.well-typed.com/intro/datatypes-and-functions.html)
- [Link to the original source of assignments A](https://teaching.well-typed.com/intro/datatypes-and-functions.html#assignments-23-assignments-a)
- [Link to the original source of assignments B](https://teaching.well-typed.com/intro/datatypes-and-functions.html#assignments-215-assignments-b)

#### Part 3 - Higher order functions

- [Link to part 3: Higher order functions](https://teaching.well-typed.com/intro/higher-order-functions.html)
- [Link to the original source of assignment C](https://teaching.well-typed.com/intro/higher-order-functions.html#assignments-37-assignments-c)

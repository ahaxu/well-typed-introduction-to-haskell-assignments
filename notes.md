### note about data vs newtype in haskell (part 02)

`data` is lazy in value constructor
`data` is not lazy in pattern matching

`newtype` is strict in value constructor
`newtype` is lazy in pattern matching (since newtype create new type which identical with the inner type)
